#' remove outliers
#'
#' remove os outliers da amostra, (que sao o primeiro e o ultimo quartil)
#' remove the outliers from the sample (that are the first and last quartile)
#' @param x values
#' @param na.rm remove NA
#' @param ... aditional parameters
#' @keywords outliers, remove, clean, data
#' @export
#' @examples
#' remove_outliers()

remove_outliers <- function(x, na.rm = TRUE, ...) {
	qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
	H <- 1.5 * IQR(x, na.rm = na.rm)
	y <- x
	y[x < (qnt[1] - H)] <- NA
	y[x > (qnt[2] + H)] <- NA
	y
}