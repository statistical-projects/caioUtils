#' copy to excel
#'
#' Copia o data.frame para um formato reconhecido pelo excel
#' Copy the data.frame to a format recognized by excel
#' @param df data.frame
#' @param sep separator (\t is tabulated)
#' @param dec decimal symbol (para o brasil tem que ser ,)
#' @param max.size limit the size of the clipboard
#' @keywords copy, excel, tab, clipboard
#' @export
#' @examples
#' copy_excel()

copy_excel <- function(df, sep="\t", dec=",", max.size=(200*1000)){
	clip <- pipe("pbcopy", "w")
	# Copy a data.frame to clipboard
	if(Sys.info()['sysname'] == "Darwin"){
		write.table(df, file=clip, sep=sep, row.names=FALSE, dec=dec)
	}else{
		write.table(df, file=clip, paste0("clipboard-", formatC(max.size, format="f", digits=0)), sep=sep, row.names=FALSE, dec=dec)
	}
	close(clip)
}